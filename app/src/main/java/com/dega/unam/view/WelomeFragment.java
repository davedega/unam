package com.dega.unam.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dega.unam.R;
import com.dega.unam.viewmodel.IWelcomeViewModel;
import com.dega.unam.viewmodel.IWelcomeViewModelListener;

public class WelomeFragment extends Fragment implements View.OnClickListener, IWelcomeViewModel {
    Button pictureBtn, galleryBtn, historyBtn;
    IWelcomeViewModelListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_welome, container, false);
        pictureBtn = (Button) rootView.findViewById(R.id.picture_btn);
        galleryBtn = (Button) rootView.findViewById(R.id.gallery_btn);
        historyBtn = (Button) rootView.findViewById(R.id.history_btn);

        pictureBtn.setOnClickListener(this);
        galleryBtn.setOnClickListener(this);
        historyBtn.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.picture_btn) {
            mListener.launchCamera();
        } else if (id == R.id.gallery_btn) {
            mListener.launchGallery();
        } else if (id == R.id.history_btn) {
            mListener.launchHistory();
        }
    }

    @Override
    public void setListener(IWelcomeViewModelListener listener) {
        mListener = listener;
    }
}
