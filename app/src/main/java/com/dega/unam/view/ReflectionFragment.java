package com.dega.unam.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dega.unam.R;
import com.dega.unam.viewmodel.IReflectionViewModel;

public class ReflectionFragment extends Fragment implements IReflectionViewModel {

    View rootView;
    LinearLayout screenShot;

    ImageView img1;
    ImageView img2;
    ImageView img3;
    ImageView img4;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_reflection, container, false);
        screenShot = (LinearLayout) rootView.findViewById(R.id.screenShot);
        img1 = (ImageView) rootView.findViewById(R.id.first_iv);
        img2 = (ImageView) rootView.findViewById(R.id.second_iv);
        img3 = (ImageView) rootView.findViewById(R.id.third_iv);
        img4 = (ImageView) rootView.findViewById(R.id.fourth_iv);

        return rootView;
    }

    @Override
    public void setFirstImage(Bitmap bitmap) {
        img1.setImageBitmap(bitmap);
    }

    @Override
    public void setSecondImage(Bitmap bitmap) {
        img2.setImageBitmap(bitmap);
    }

    @Override
    public void setThirdImage(Bitmap bitmap) {
        img3.setImageBitmap(bitmap);
    }

    @Override
    public void setFourthImage(Bitmap bitmap) {
        img4.setImageBitmap(bitmap);
    }

    @Override
    public void setAllImages(Bitmap bm1, Bitmap bm2, Bitmap bm3, Bitmap bm4) {
        img1.setImageBitmap(bm1);
        img2.setImageBitmap(bm2);
        img3.setImageBitmap(bm3);
        img4.setImageBitmap(bm4);
    }

    @Override
    public Bitmap getScreenShot() {
        screenShot.setDrawingCacheEnabled(true);
        Bitmap bitmap;
        bitmap = Bitmap.createBitmap(screenShot.getDrawingCache());
        screenShot.setDrawingCacheEnabled(false);



        return bitmap;

    }


}
