package com.dega.unam.viewmodel;

import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;

/**
 * Created by miziloner on 18/07/16.
 */
public interface IReflectionViewModel {

    void setFirstImage(Bitmap bitmap);

    void setSecondImage(Bitmap bitmap);

    void setThirdImage(Bitmap bitmap);

    void setFourthImage(Bitmap bitmap);

    void setAllImages(Bitmap bm1, Bitmap bm2, Bitmap bm3, Bitmap bm4);

    Bitmap getScreenShot();

}
