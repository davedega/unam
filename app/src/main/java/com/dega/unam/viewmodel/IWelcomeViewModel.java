package com.dega.unam.viewmodel;

/**
 * Created by miziloner on 17/07/16.
 */
public interface IWelcomeViewModel {
    void setListener(IWelcomeViewModelListener listener);
}
