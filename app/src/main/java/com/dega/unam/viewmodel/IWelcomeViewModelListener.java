package com.dega.unam.viewmodel;

/**
 * Created by miziloner on 17/07/16.
 */
public interface IWelcomeViewModelListener {

    void launchCamera();
    void launchGallery();
    void launchHistory();
}
