package com.dega.unam.controller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.dega.unam.R;
import com.dega.unam.UnamCamera;
import com.dega.unam.viewmodel.IReflectionViewModel;
import com.frosquivel.magicalcamera.MagicalCamera;

public class ReflectionActivity extends AppCompatActivity {
    IReflectionViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reflection);
        viewModel = (IReflectionViewModel) getSupportFragmentManager().findFragmentById(R.id.fragment);
//        Saco Extras con compresion
        if (getIntent().hasExtra("byteArray")) {
            Bitmap originalImage = BitmapFactory.decodeByteArray(
                    getIntent().getByteArrayExtra("byteArray"), 0, getIntent().getByteArrayExtra("byteArray").length);

            viewModel.setFirstImage(originalImage);


            Matrix matrix = new Matrix();
            matrix.postRotate(-90);
            Bitmap secondRotated = Bitmap.createBitmap(originalImage, 0, 0,originalImage.getWidth(), originalImage.getHeight(), matrix, true);
            viewModel.setSecondImage(secondRotated);
            matrix.postRotate(-90);
            Bitmap thirdRotated = Bitmap.createBitmap(secondRotated, 0, 0,secondRotated.getWidth(), secondRotated.getHeight(), matrix, true);

            viewModel.setThirdImage(thirdRotated);

            matrix.postRotate(360);
            Bitmap fourthRotated = Bitmap.createBitmap(originalImage, 0, 0,originalImage.getWidth(), originalImage.getHeight(), matrix, true);

            viewModel.setFourthImage(fourthRotated);

//            matrix.postRotate(90);
//            Bitmap thirdRotated = Bitmap.createBitmap(originalImage, 0, 0, originalImage.getWidth(), originalImage.getHeight(), matrix, true);
//            matrix.postRotate(90);
//            Bitmap fourthRotated = Bitmap.createBitmap(originalImage, 0, 0, originalImage.getWidth(), originalImage.getHeight(), matrix, true);
//            viewModel.setSecondImage(secondRotated);
//            viewModel.setThirdImage(thirdRotated);
//            viewModel.setFourthImage(fourthRotated);
//            takeScreenShot();
        }
    }

    public void takeScreenShot() {
        Bitmap screenShot = viewModel.getScreenShot();
        UnamCamera magicalCamera = new UnamCamera(this, 200);
        if (magicalCamera.savePhotoInMemoryDevice(screenShot, "refelction", MagicalCamera.JPEG, true)) {
            Toast.makeText(this, "The photo is save in device, please check this", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Sorry your photo dont write in devide, please contact with dave.dega@gmail and say this error", Toast.LENGTH_SHORT).show();
        }
    }
}
