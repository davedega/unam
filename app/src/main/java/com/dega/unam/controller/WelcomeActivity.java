package com.dega.unam.controller;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.dega.unam.R;
import com.dega.unam.UnamCamera;
import com.dega.unam.viewmodel.IWelcomeViewModel;
import com.dega.unam.viewmodel.IWelcomeViewModelListener;
import com.frosquivel.magicalcamera.MagicalCamera;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import nl.changer.polypicker.ImagePickerActivity;

public class WelcomeActivity extends AppCompatActivity implements IWelcomeViewModelListener {
    IWelcomeViewModel viewModel;
    Context mContext;
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 3000;
    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    private static final int REQUEST_PERMISSIONS = 200;
    private UnamCamera magicalCamera;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wellcome);
        mContext = getApplicationContext();
        magicalCamera = new UnamCamera(this, RESIZE_PHOTO_PIXELS_PERCENTAGE);
        viewModel = (IWelcomeViewModel) getSupportFragmentManager().findFragmentById(R.id.fragment);
        viewModel.setListener(this);
        requestMMPermissions();
    }


    private void requestMMPermissions() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            String writeStoragePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
            String readStoragePermission = Manifest.permission.READ_EXTERNAL_STORAGE;
            String cameraPermission = Manifest.permission.CAMERA;


            int hasWritePermission = checkSelfPermission(writeStoragePermission);
            int hasReadPermission = checkSelfPermission(readStoragePermission);
            int hasCameraPermission = checkSelfPermission(cameraPermission);

            List<String> permissions = new ArrayList<String>();

            if (hasWritePermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(writeStoragePermission);
            }
            if (hasReadPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(readStoragePermission);
            }
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(cameraPermission);
            }

            if (!permissions.isEmpty()) {
                final String[] params = permissions.toArray(new String[permissions.size()]);
                requestPermissions(params, REQUEST_PERMISSIONS);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        magicalCamera.resultPhoto(requestCode, resultCode, data);

        if (magicalCamera.getMyPhoto() != null) {
//            imageView.setImageBitmap(magicalCamera.getMyPhoto());
                Bitmap picture = magicalCamera.getMyPhoto();
                Bitmap smallPicture = magicalCamera.resizePhoto(picture, getSizeSquare(), false);
                Intent i = new Intent(this, ReflectionActivity.class);
//                i.putExtra("Image", smallPicture);

//                Con Compresion
                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                smallPicture.compress(Bitmap.CompressFormat.PNG, 50, bs);
                i.putExtra("byteArray", bs.toByteArray());
                startActivity(i);
        } else {
//            Toast.makeText(this, "Your image is null, please debug, or test with another device, or maybe contact with fabian7593@gmail.com for try to fix the bug, thanks and sorry", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void launchCamera() {
        Log.i("unam", "launchCamera");
        magicalCamera.takePhoto();

    }

    @Override
    public void launchGallery() {
        magicalCamera.selectedPicture("my_header_name");

    }

    @Override
    public void launchHistory() {
        Log.i("unam", "launchHistory");

    }

    public int getWidthScreen(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return width;
    }

    public int getHeightScreen(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return height;
    }

    public int getSizeSquare(){
        int width = getWidthScreen();
        int size = width/3;
        return size;
    }
}
